CREATE DATABASE banks;

USE banks;

CREATE TABLE clients (
     client_id INT NOT NULL AUTO_INCREMENT,
     first_name CHAR(50) NOT NULL,
     last_name CHAR(50) NOT NULL,
     PRIMARY KEY (client_id)
 );

 CREATE TABLE accounts (
     account_id INT NOT NULL AUTO_INCREMENT,
     balance BIGINT NOT NULL,
     account_type TINYINT NOT NULL,
     nb_operations BIGINT NOT NULL,
     client_id INT NOT NULL,
     PRIMARY KEY (account_id),
     FOREIGN KEY (client_id) REFERENCES clients(client_id)
 );