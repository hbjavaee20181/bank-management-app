package com.humanbooster.bankmanagementapp.sql;

import com.humanbooster.bankmanagementapp.constants.SQLConstants;
import com.humanbooster.bankmanagementapp.utils.SqlUtils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by Ben on 06/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public class ConnectionManager {

    private ConnectionManager() {

    }

    private static Connection connection;

    public static boolean initConnection() {
        SqlUtils.loadDriver();
        return getConnection() != null;
    }

    // Warning, not totally thread safe
    public static Connection getConnection() {
        if (connection == null) {
            try {
                connection = DriverManager.getConnection(
                        SQLConstants.DB_URL,
                        SQLConstants.DB_USER,
                        SQLConstants.DB_PASSWORD
                );

                // Disable auto commit
                connection.setAutoCommit(false);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return connection;
    }

    public static void closeConnection() {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
