package com.humanbooster.bankmanagementapp.exceptions;

/**
 * Created by Ben on 28/05/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public class BankAccountNotExistsException extends RuntimeException {

    public BankAccountNotExistsException(Integer accountId) {
        super("L'id de compte " + accountId + " n'existe pas");
    }
}
