package com.humanbooster.bankmanagementapp.exceptions;

/**
 * Created by Ben on 28/05/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public class ClientNotExistsException extends RuntimeException {

    public ClientNotExistsException(Integer clientId) {
        super("L'id client " + clientId + " n'existe pas");
    }
}