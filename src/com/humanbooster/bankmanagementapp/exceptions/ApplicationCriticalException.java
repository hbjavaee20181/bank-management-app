package com.humanbooster.bankmanagementapp.exceptions;

/**
 * Created by Ben on 30/05/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public class ApplicationCriticalException extends RuntimeException {
    public ApplicationCriticalException(Throwable cause) {
        super("Désolé, une erreur s'est produite et l'application va se fermer. " + cause.getMessage(), cause);
    }
}
