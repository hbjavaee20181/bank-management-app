package com.humanbooster.bankmanagementapp.exceptions;

/**
 * Created by Ben on 28/05/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public class BadInputException extends Exception {

    public BadInputException() {
        super();
    }

    public BadInputException(Throwable cause) {
        super(cause);
    }
}
