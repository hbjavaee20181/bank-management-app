package com.humanbooster.bankmanagementapp.model;

/**
 * Created by Ben on 21/05/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public enum AccountType {
    REGULAR,
    LIVRET_A,
    LDD,
    LIFE_INSURANCE
}