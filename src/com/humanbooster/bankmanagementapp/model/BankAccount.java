package com.humanbooster.bankmanagementapp.model;

import java.io.Serializable;
import java.util.Objects;

/**
 * Created by Ben on 21/05/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public class BankAccount implements Serializable {

    private Integer id;
    private Long balance = 0L;
    private AccountType type;
    private Long nbOperations = 0L;


    public BankAccount() {

    }

    public BankAccount(Long balance, AccountType accountType) {
        this(null, balance, accountType, 0L);
    }

    public BankAccount(Integer id, Long balance, AccountType accountType) {
        this(id, balance, accountType, 0L);
    }

    public BankAccount(Integer id, Long balance, AccountType type, Long nbOperations) {
        this.id = id;
        this.balance = balance;
        this.type = type;
        this.nbOperations = nbOperations;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer number) {
        this.id = id;
    }

    public Long getBalance() {
        return balance;
    }

    public void setBalance(Long balance) {
        this.balance = balance;
    }

    public AccountType getType() {
        return type;
    }

    public void setType(AccountType type) {
        this.type = type;
    }

    public Long getNbOperations() {
        return nbOperations;
    }

    public void setNbOperations(Long nbOperations) {
        this.nbOperations = nbOperations;
    }

    public void updateMoney(Integer amount) {
        incrementNbOperations();
        balance += amount;
    }

    private void incrementNbOperations() {
        nbOperations++;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BankAccount account = (BankAccount) o;
        return Objects.equals(getId(), account.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
