package com.humanbooster.bankmanagementapp;

import com.humanbooster.bankmanagementapp.exceptions.ApplicationCriticalException;
import com.humanbooster.bankmanagementapp.sql.ConnectionManager;

public class Main {

    public static void main(String[] args) {

        boolean isConnectionsInitialized = ConnectionManager.initConnection();

        if (!isConnectionsInitialized) {
            System.out.println("Cannot launch the application. Cannot initialized DB Connection.");
            return;
        }
        Application app = new Application();

        try {
            app.launch();
        } catch (ApplicationCriticalException e) {
            System.out.println(e.getMessage());
        }
    }
}
