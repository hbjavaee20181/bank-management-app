package com.humanbooster.bankmanagementapp.constants;

/**
 * Created by Ben on 06/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public final class SQLConstants {

    private SQLConstants() {

    }

    public static final String DB_URL = "jdbc:mariadb://localhost:32769/banks";

    public static final String DB_USER = "root";

    public static final String DB_PASSWORD = "root";
}
