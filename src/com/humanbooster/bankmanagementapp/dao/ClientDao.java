package com.humanbooster.bankmanagementapp.dao;

import com.humanbooster.bankmanagementapp.model.Client;

import java.util.List;

/**
 * Created by Ben on 06/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public interface ClientDao {

    List<Client> getClients();

    void addClient(Client client);

    Client getClient(Integer clientId);
}
