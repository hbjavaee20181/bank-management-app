package com.humanbooster.bankmanagementapp.dao;

import com.humanbooster.bankmanagementapp.dao.impl.SqlBankAccountDao;
import com.humanbooster.bankmanagementapp.dao.impl.SqlClientDao;
import com.humanbooster.bankmanagementapp.sql.ConnectionManager;

/**
 * Created by Ben on 25/05/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public class DaoFactory {

    public static ClientDao getClientDao() {
        return new SqlClientDao(ConnectionManager.getConnection());
    }

    public static BankAccountDao getBankAccountDao() {
        return new SqlBankAccountDao(ConnectionManager.getConnection());
    }
}
