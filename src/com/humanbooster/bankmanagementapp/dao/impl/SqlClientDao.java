package com.humanbooster.bankmanagementapp.dao.impl;

import com.humanbooster.bankmanagementapp.dao.ClientDao;
import com.humanbooster.bankmanagementapp.exceptions.ApplicationCriticalException;
import com.humanbooster.bankmanagementapp.exceptions.ClientNotExistsException;
import com.humanbooster.bankmanagementapp.model.Client;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ben on 06/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public class SqlClientDao implements ClientDao {

    private final Connection connection;

    public SqlClientDao(Connection connection) {
        this.connection = connection;
    }

    @Override
    public List<Client> getClients() {
        try {
            PreparedStatement stmt = connection.prepareStatement(
                    "SELECT client_id, first_name, last_name FROM clients");
            ResultSet rs = stmt.executeQuery();

            List<Client> clients = new ArrayList<>();

            while (rs.next()) {
                int clientId = rs.getInt(1);
                String firstName = rs.getString(2);
                String lastName = rs.getString(3);

                Client c = new Client(clientId, firstName, lastName);

                clients.add(c);
            }

            return clients;
        } catch (SQLException e) {
            throw new ApplicationCriticalException(e);
        }
    }

    @Override
    public void addClient(Client client) {
        try {
            PreparedStatement stmt = connection.prepareStatement(
                    "INSERT INTO clients(first_name, last_name) VALUES (?, ?)");
            stmt.setString(1, client.getFirstName());
            stmt.setString(2, client.getLastName());

            stmt.executeUpdate();

            commit();
        } catch (SQLException e) {
            rollback();
            throw new ApplicationCriticalException(e);
        }
    }

    @Override
    public Client getClient(Integer clientId) {
        try {
            PreparedStatement stmt = connection.prepareStatement(
                    "SELECT client_id, first_name, last_name FROM clients WHERE client_id = ?");
            stmt.setInt(1, clientId);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                String firstName = rs.getString(2);
                String lastName = rs.getString(3);
                return new Client(clientId, firstName, lastName);
            }
            throw new ClientNotExistsException(clientId);
        } catch (SQLException e) {
            throw new ApplicationCriticalException(e);
        }
    }

    private void rollback() {
        try {
            connection.rollback();
        } catch (SQLException e) {
            throw new ApplicationCriticalException(e);
        }
    }

    private void commit() {
        try {
            connection.commit();
        } catch (SQLException e) {
            throw new ApplicationCriticalException(e);
        }
    }
}
