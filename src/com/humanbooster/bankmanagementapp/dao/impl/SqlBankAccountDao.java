package com.humanbooster.bankmanagementapp.dao.impl;

import com.humanbooster.bankmanagementapp.dao.BankAccountDao;
import com.humanbooster.bankmanagementapp.exceptions.ApplicationCriticalException;
import com.humanbooster.bankmanagementapp.exceptions.BankAccountNotExistsException;
import com.humanbooster.bankmanagementapp.model.AccountType;
import com.humanbooster.bankmanagementapp.model.BankAccount;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ben on 06/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public class SqlBankAccountDao implements BankAccountDao {

    private final Connection connection;

    public SqlBankAccountDao(Connection connection) {
        this.connection = connection;
    }

    @Override
    public void addAccount(Integer clientId, BankAccount account) {
        try {
            PreparedStatement stmt = connection.prepareStatement(
                    "INSERT INTO accounts(balance, account_type, nb_operations, client_id) VALUES (?, ?, ?, ?)");
            stmt.setLong(1, account.getBalance());
            stmt.setInt(2, account.getType().ordinal());
            stmt.setLong(3, account.getNbOperations());
            stmt.setInt(4, clientId);

            stmt.executeUpdate();

            commit();
        } catch (SQLException e) {
            rollback();
            throw new ApplicationCriticalException(e);
        }
    }

    @Override
    public List<BankAccount> getClientAccounts(Integer clientId) {
        try {
            PreparedStatement stmt = connection.prepareStatement(
                    "SELECT account_id, balance, account_type, nb_operations " +
                            "FROM accounts WHERE client_id = ?");
            stmt.setLong(1, clientId);

            ResultSet rs = stmt.executeQuery();

            List<BankAccount> accounts = new ArrayList<>();

            while (rs.next()) {
                int accountId = rs.getInt(1);
                Long balance = rs.getLong(2);
                AccountType type = AccountType.values()[rs.getInt(3)];
                Long nbOps = rs.getLong(4);

                BankAccount a = new BankAccount(accountId, balance, type, nbOps);

                accounts.add(a);
            }

            return accounts;
        } catch (SQLException e) {
            throw new ApplicationCriticalException(e);
        }
    }

    @Override
    public BankAccount getClientAccount(Integer clientId, Integer accountId) {
        try {
            PreparedStatement stmt = connection.prepareStatement(
                    "SELECT account_id, balance, account_type, nb_operations " +
                            "FROM accounts WHERE client_id = ? AND account_id = ?");
            stmt.setLong(1, clientId);
            stmt.setLong(2, accountId);

            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                Long balance = rs.getLong(2);
                AccountType type = AccountType.values()[rs.getInt(3)];
                Long nbOps = rs.getLong(4);

                return new BankAccount(accountId, balance, type, nbOps);
            }

            throw new BankAccountNotExistsException(accountId);
        } catch (SQLException e) {
            throw new ApplicationCriticalException(e);
        }
    }

    @Override
    public void withdrawMoney(Integer clientId, Integer accountId, Integer money) {
        try {
            PreparedStatement stmt = connection.prepareStatement(
                    "UPDATE accounts SET balance = balance - ?, nb_operations = nb_operations + 1  " +
                            "WHERE client_id = ? AND account_id = ?");
            stmt.setLong(1, money);
            stmt.setLong(2, clientId);
            stmt.setLong(3, accountId);

            stmt.executeUpdate();

            commit();
        } catch (SQLException e) {
            rollback();
            throw new ApplicationCriticalException(e);
        }
    }

    @Override
    public void addMoney(Integer clientId, Integer accountId, Integer money) {
        try {
            PreparedStatement stmt = connection.prepareStatement(
                    "UPDATE accounts SET balance = balance + ?, nb_operations = nb_operations + 1 " +
                            "WHERE client_id = ? AND account_id = ?");
            stmt.setLong(1, money);
            stmt.setLong(2, clientId);
            stmt.setLong(3, accountId);

            stmt.executeUpdate();

            commit();
        } catch (SQLException e) {
            rollback();
            throw new ApplicationCriticalException(e);
        }
    }

    private void rollback() {
        try {
            connection.rollback();
        } catch (SQLException e) {
            throw new ApplicationCriticalException(e);
        }
    }

    private void commit() {
        try {
            connection.commit();
        } catch (SQLException e) {
            throw new ApplicationCriticalException(e);
        }
    }
}
