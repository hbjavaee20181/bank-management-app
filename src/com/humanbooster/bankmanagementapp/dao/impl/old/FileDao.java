package com.humanbooster.bankmanagementapp.dao.impl.old;

/**
 * Created by Ben on 30/05/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public class FileDao {//implements Dao {

    /*private static final String FILE_PATH = "/Users/Ben/baa_save.dat";

    @Override
    public List<Client> getClients() {
        return getClientsFromFile();
    }

    @Override
    public void addClient(Client client) {
        List<Client> clients = getClients();
        clients.add(client);
        storeClientsListToFile(clients);
    }

    @Override
    public void addAccount(String clientId, BankAccount account) {
        List<Client> clients = getClients();
        Client client = clients.stream()
                .filter(c -> c.getId().equals(clientId))
                .findFirst()
                .orElseThrow(() -> new ClientNotExistsException(clientId));
        client.getAccounts().add(account);
        storeClientsListToFile(clients);
    }

    @Override
    public List<BankAccount> getClientAccounts(String clientId) {
        Client client = getClient(clientId);
        return client.getAccounts();
    }

    @Override
    public BankAccount getClientAccount(String clientId, String accountId) {
        List<BankAccount> accounts = getClientAccounts(clientId);
        for (BankAccount account : accounts) {
            if (account.getId().equals(accountId)) {
                return account;
            }
        }
        throw new BankAccountNotExistsException(accountId);
    }

    @Override
    public Client getClient(String clientId) {

        for (Client client : getClients()) {
            if (client.getId().equals(clientId)) {
                return client;
            }
        }
        throw new ClientNotExistsException(clientId);
    }

    @Override
    public void withdrawMoney(String clientId, String accountId, Integer money) {
        updateMoney(clientId, accountId, money * -1);
    }

    @Override
    public void addMoney(String clientId, String accountId, Integer money) {
        updateMoney(clientId, accountId, money);
    }

    private void updateMoney(String clientId, String accountId, Integer amount) {
        List<Client> clients = getClients();

        BankAccount acc = clients.stream()
                .filter(c -> c.getId().equals(clientId))
                .map(Client::getAccounts)
                .flatMap(Collection::stream)
                .filter(a -> a.getId().equals(accountId))
                .findFirst()
                .orElseThrow(() -> new BankAccountNotExistsException(accountId));

        acc.updateMoney(amount);
        storeClientsListToFile(clients);
    }

    private boolean isStorageFileExists() {
        Path path = Paths.get(FILE_PATH);
        return Files.exists(path);
    }

    @SuppressWarnings("unchecked")
    private List<Client> getClientsFromFile() {
        if (!isStorageFileExists()) {
            initStorageFile();
        }
        try (ObjectInputStream is = new ObjectInputStream(new FileInputStream(FILE_PATH))) {
            return (List<Client>) is.readObject();
        } catch (IOException | ClassNotFoundException e) {
            throw new ApplicationCriticalException(e);
        }
    }

    private void storeClientsListToFile(List<Client> clients) {
        if (!isStorageFileExists()) {
            initStorageFile();
        }
        try (ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(FILE_PATH))) {
            os.writeObject(clients);
            log("Clients list successfully updated !");
        } catch (IOException e) {
            throw new ApplicationCriticalException(e);
        }
    }

    private void initStorageFile() {
        log("The storage file does not exists. Creating ...");
        createBaseFile();
        log("Storage file created successfully !");
        storeEmptyListInFile();
        log("Storage file initialized successfully !");
    }

    private void storeEmptyListInFile() {
        List<Client> clients = new ArrayList<>();
        storeClientsListToFile(clients);
    }

    private void createBaseFile() {
        Path path = Paths.get(FILE_PATH);
        try {
            Files.createFile(path);
        } catch (IOException e) {
            throw new ApplicationCriticalException(e);
        }
    }


    private void log(String message) {
        System.out.println("LOGGER : " + message);
    }*/
}
