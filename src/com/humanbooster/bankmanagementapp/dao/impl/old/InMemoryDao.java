package com.humanbooster.bankmanagementapp.dao.impl.old;

/**
 * Created by Ben on 22/05/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public class InMemoryDao {//implements Dao {
/*
    private List<Client> clients = new ArrayList<>();

    @Override
    public List<Client> getClients() {
        return clients;
    }

    @Override
    public void addClient(Client client) {
        getClients().add(client);
    }

    @Override
    public void addAccount(String clientId, BankAccount account) {
        Client clientToUpdate = getClient(clientId);
        clientToUpdate.getAccounts().add(account);
    }

    @Override
    public List<BankAccount> getClientAccounts(String clientId) {
        Client client = getClient(clientId);
        return client.getAccounts();
    }

    @Override
    public BankAccount getClientAccount(String clientId, String accountId) {
        List<BankAccount> accounts = getClientAccounts(clientId);
        for (BankAccount account : accounts) {
            if (account.getId().equals(accountId)) {
                return account;
            }
        }
        throw new BankAccountNotExistsException(accountId);
    }

    @Override
    public Client getClient(String clientId) {

        for (Client client : getClients()) {
            if (client.getId().equals(clientId)) {
                return client;
            }
        }
        throw new ClientNotExistsException(clientId);
    }

    @Override
    public void withdrawMoney(String clientId, String accountId, Integer money) {
        updateMoney(clientId, accountId, money * -1);
    }

    @Override
    public void addMoney(String clientId, String accountId, Integer money) {
        updateMoney(clientId, accountId, money);
    }

    private void updateMoney(String clientId, String accountId, Integer amount) {
        BankAccount account = getClientAccount(clientId, accountId);
        account.updateMoney(amount);
    }*/
}
