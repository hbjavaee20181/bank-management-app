package com.humanbooster.bankmanagementapp.dao;

import com.humanbooster.bankmanagementapp.model.BankAccount;

import java.util.List;

/**
 * Created by Ben on 06/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public interface BankAccountDao {

    void addAccount(Integer clientId, BankAccount account);

    List<BankAccount> getClientAccounts(Integer clientId);

    BankAccount getClientAccount(Integer clientId, Integer accountId);

    void withdrawMoney(Integer clientId, Integer accountId, Integer money);

    void addMoney(Integer clientId, Integer accountId, Integer money);
}
