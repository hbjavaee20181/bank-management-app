package com.humanbooster.bankmanagementapp.utils;

import com.humanbooster.bankmanagementapp.exceptions.BadInputException;

import java.util.Random;
import java.util.Scanner;
import java.util.UUID;

/**
 * Created by Ben on 18/05/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public class Tools {

    public static Integer getConsoleNumber() throws BadInputException {
        try {
            return Integer.parseInt(getConsoleString());
        } catch (NumberFormatException e) {
            throw new BadInputException(e);
        }
    }

    public static String getConsoleString() {
        Scanner sc = new Scanner(System.in);
        return sc.nextLine();
    }

    public static String generateUUID() {
        return UUID.randomUUID().toString();
    }

    public static Integer generateRandomNumber100() {
        return generateRandomNumber(100);
    }

    public static Integer generateRandomNumber(Integer maxRange) {
        return new Random().nextInt(maxRange);
    }
}
