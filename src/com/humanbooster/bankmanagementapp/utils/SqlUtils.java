package com.humanbooster.bankmanagementapp.utils;

/**
 * Created by Ben on 06/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public final class SqlUtils {

    private SqlUtils() {
    }

    public static void loadDriver() {
        try {
            Class.forName(org.mariadb.jdbc.Driver.class.getName());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
