package com.humanbooster.bankmanagementapp;

import com.humanbooster.bankmanagementapp.exceptions.BadInputException;
import com.humanbooster.bankmanagementapp.utils.Tools;

/**
 * Created by Ben on 21/05/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public class Console {

    public void printOpeningMessage() {
        printMessage("---- Bienvenue cher conseiller ----");
    }

    public void printClosingMessage() {
        printMessage("---- A bientôt ! ----");
    }

    public void printMenu() {
        printConsoleLine();
        printMessage("Que voulez-vous faire ?");
        printMessage("1 - Lister vos clients");
        printMessage("2 - Lister les comptes d'un client");
        printMessage("3 - Ajouter un client");
        printMessage("4 - Ajouter un compte pour un client");
        printMessage("5 - Effetuer un retrait sur un compte");
        printMessage("6 - Effectuer un dépôt sur un compte");
        printMessage("7 - Afficher les détails d'un compte");
        printConsoleLine();
    }

    public void printMessage(String message) {
        System.out.println(message);
    }

    public void printConsoleLine() {
        System.out.println("___________________________________");
    }

    public Integer printMessageAndGetConsoleNumber(String message) {
        printMessage(message);
        try {
            return Tools.getConsoleNumber();
        } catch (BadInputException e) {
            printMessage("Je n'ai pas compris :(");
            return printMessageAndGetConsoleNumber(message);
        }
    }

    public String printMessageAndGetConsoleString(String message) {
        printMessage(message);
        return Tools.getConsoleString();
    }
}
