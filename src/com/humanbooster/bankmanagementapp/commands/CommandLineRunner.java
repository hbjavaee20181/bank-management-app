package com.humanbooster.bankmanagementapp.commands;

import com.humanbooster.bankmanagementapp.Console;

/**
 * Created by Ben on 29/05/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public abstract class CommandLineRunner {

    protected Console console;

    public CommandLineRunner() {
        console = new Console();
    }

    public abstract void execute();
}
