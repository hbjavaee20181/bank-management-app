package com.humanbooster.bankmanagementapp.commands.impl;

import com.humanbooster.bankmanagementapp.commands.CommandLineRunner;
import com.humanbooster.bankmanagementapp.dao.BankAccountDao;
import com.humanbooster.bankmanagementapp.dao.ClientDao;
import com.humanbooster.bankmanagementapp.dao.DaoFactory;
import com.humanbooster.bankmanagementapp.model.BankAccount;
import com.humanbooster.bankmanagementapp.model.Client;

import java.util.List;

/**
 * Created by Ben on 29/05/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public class DisplayAccountDetailsCommandLineRunner extends CommandLineRunner {
    @Override
    public void execute() {
        ClientDao clientDao = DaoFactory.getClientDao();
        BankAccountDao bankAccountDao = DaoFactory.getBankAccountDao();
        console.printMessage("---- Vos Clients ----");
        List<Client> clients = clientDao.getClients();
        if (clients.isEmpty()) {
            console.printMessage("Il n'y a aucun client dans votre base de données");
            return;
        }
        for (Client client : clients) {
            console.printMessage("- " + client.getFirstName() + " " + client.getLastName() + " - " + client.getId());
        }
        Integer clientId = console.printMessageAndGetConsoleNumber("Quel client choisissez-vous ?");
        List<BankAccount> accounts = bankAccountDao.getClientAccounts(clientId);
        if (accounts.isEmpty()) {
            console.printMessage("Ce client ne possède pas de compte !");
            return;
        }
        for (BankAccount account : accounts) {
            console.printMessage("- " + account.getId() + " " + account.getType());
        }
        Integer accountId = console.printMessageAndGetConsoleNumber("Quel compte choisissez-vous ?");
        BankAccount account = bankAccountDao.getClientAccount(clientId, accountId);
        console.printConsoleLine();
        console.printMessage("---- Compte numéro " + account.getId() + " ----");
        console.printMessage("Type de compte : " + account.getType());
        console.printMessage("Solde : " + account.getBalance() + " €");
        console.printMessage("Nombre d'opérations : " + account.getNbOperations());
        console.printConsoleLine();
    }
}
