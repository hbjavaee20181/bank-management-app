package com.humanbooster.bankmanagementapp.commands.impl;

import com.humanbooster.bankmanagementapp.commands.CommandLineRunner;
import com.humanbooster.bankmanagementapp.dao.DaoFactory;
import com.humanbooster.bankmanagementapp.model.Client;

import java.util.List;

/**
 * Created by Ben on 29/05/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public class ListClientCommandLineRunner extends CommandLineRunner {

    @Override
    public void execute() {
        List<Client> clients = DaoFactory.getClientDao().getClients();
        if (clients.isEmpty()) {
            console.printMessage("Il n'y a actuellement aucun clients dans votre base");
            return;
        }
        for (Client client : clients) {
            console.printConsoleLine();
            console.printMessage("\t" + client.getFirstName() + " " + client.getLastName() + " - " + client.getId());
            console.printMessage("\tNumber of accounts : " + client.getAccounts().size());
        }
    }

}
