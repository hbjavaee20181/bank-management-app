package com.humanbooster.bankmanagementapp.commands.impl;

import com.humanbooster.bankmanagementapp.commands.CommandLineRunner;
import com.humanbooster.bankmanagementapp.dao.DaoFactory;
import com.humanbooster.bankmanagementapp.model.Client;

/**
 * Created by Ben on 29/05/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public class AddClientCommandLineRunner extends CommandLineRunner {
    @Override
    public void execute() {
        String firstName = console.printMessageAndGetConsoleString("Quel est le prénom ?");
        String lastName = console.printMessageAndGetConsoleString("Quel est le nom ?");

        Client newClient = new Client(firstName, lastName);

        DaoFactory.getClientDao().addClient(newClient);
    }
}
