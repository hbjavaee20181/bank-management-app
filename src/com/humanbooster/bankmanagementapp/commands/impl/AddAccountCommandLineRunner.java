package com.humanbooster.bankmanagementapp.commands.impl;

import com.humanbooster.bankmanagementapp.commands.CommandLineRunner;
import com.humanbooster.bankmanagementapp.dao.BankAccountDao;
import com.humanbooster.bankmanagementapp.dao.ClientDao;
import com.humanbooster.bankmanagementapp.dao.DaoFactory;
import com.humanbooster.bankmanagementapp.model.AccountType;
import com.humanbooster.bankmanagementapp.model.BankAccount;
import com.humanbooster.bankmanagementapp.model.Client;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Ben on 29/05/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public class AddAccountCommandLineRunner extends CommandLineRunner {
    @Override
    public void execute() {
        ClientDao clientDao = DaoFactory.getClientDao();
        BankAccountDao bankAccountDao = DaoFactory.getBankAccountDao();
        console.printMessage("---- Vos Clients ----");
        List<Client> clients = clientDao.getClients();
        if (clients.isEmpty()) {
            console.printMessage("Il n'y a aucun client dans votre base de données");
            return;
        }
        for (Client client : clients) {
            console.printMessage("- " + client.getFirstName() + " " + client.getLastName() + " - " + client.getId());
        }
        Integer clientId = console.printMessageAndGetConsoleNumber("Pour quel client voulez-vous ajouter un compte ?");

        String availableAccountTypes = Stream.of(AccountType.values())
                .map(Enum::name)
                .collect(Collectors.joining(","));

        String accountTypeSelected = console.printMessageAndGetConsoleString("Quel est le type du nouveau compte ? ("
                + availableAccountTypes + ")");

        AccountType accountType;
        try {
            accountType = AccountType.valueOf(accountTypeSelected);
        } catch (IllegalArgumentException e) {
            console.printMessageAndGetConsoleString("Type de compte inconnu. Opération annulée.");
            return;
        }

        Integer balance = console.printMessageAndGetConsoleNumber("Quel est le solde du compte ?");

        BankAccount account = new BankAccount(
                balance.longValue(),
                accountType
        );

        bankAccountDao.addAccount(clientId, account);
    }
}
