package com.humanbooster.bankmanagementapp.commands.impl;

import com.humanbooster.bankmanagementapp.commands.CommandLineRunner;
import com.humanbooster.bankmanagementapp.dao.BankAccountDao;
import com.humanbooster.bankmanagementapp.dao.ClientDao;
import com.humanbooster.bankmanagementapp.dao.DaoFactory;
import com.humanbooster.bankmanagementapp.model.Client;

import java.util.List;

/**
 * Created by Ben on 29/05/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public class ListClientAccountsCommandLineRunner extends CommandLineRunner {
    @Override
    public void execute() {
        ClientDao clientDao = DaoFactory.getClientDao();
        BankAccountDao bankAccountDao = DaoFactory.getBankAccountDao();
        console.printMessage("---- Vos Clients ----");
        List<Client> clients = clientDao.getClients();
        if (clients.isEmpty()) {
            console.printMessage("Il n'y a actuellement aucun clients dans votre base");
            return;
        }
        clients.forEach(c -> console.printMessage("- " + c.getFirstName() + " " + c.getLastName() + " - " + c.getId()));

        Integer clientId = console.printMessageAndGetConsoleNumber("Quel client choisissez-vous ?");

        bankAccountDao.getClientAccounts(clientId).forEach(account -> {
            console.printConsoleLine();
            console.printMessage("---- Compte numéro " + account.getId() + " ----");
            console.printMessage("Type de compte : " + account.getType());
            console.printMessage("Solde : " + account.getBalance() + " €");
            console.printMessage("Nombre d'opérations : " + account.getNbOperations());
            console.printConsoleLine();
        });
    }
}
