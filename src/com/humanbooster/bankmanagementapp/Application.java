package com.humanbooster.bankmanagementapp;

import com.humanbooster.bankmanagementapp.commands.CommandLineRunner;
import com.humanbooster.bankmanagementapp.commands.impl.*;
import com.humanbooster.bankmanagementapp.exceptions.BankAccountNotExistsException;
import com.humanbooster.bankmanagementapp.exceptions.ClientNotExistsException;
import com.humanbooster.bankmanagementapp.sql.ConnectionManager;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Ben on 21/05/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public class Application {

    private Console console;
    private Boolean isLaunch = true;
    private Boolean isFirstLoop = true;

    private Map<Integer, CommandLineRunner> commands = new HashMap<>();

    public Application() {
        commands.put(1, new ListClientCommandLineRunner());
        commands.put(2, new ListClientAccountsCommandLineRunner());
        commands.put(3, new AddClientCommandLineRunner());
        commands.put(4, new AddAccountCommandLineRunner());
        commands.put(5, new DecreaseAccountCommandLineRunner());
        commands.put(6, new IncreaseAccountCommandLineRunner());
        commands.put(7, new DisplayAccountDetailsCommandLineRunner());
    }


    private boolean shouldPrintMenu() {
        if (isFirstLoop) {
            isFirstLoop = false;
            return true;
        }
        String response = console.printMessageAndGetConsoleString("Voulez-vous effectuer une autre opération ? (O/N)");
        return response.equalsIgnoreCase("O");
    }

    public void launch() {

        // Instanciation de l'objet console
        console = new Console();

        // Affichage du message de bienvenue
        console.printOpeningMessage();

        while (isLaunch) {
            // Affichage du menu selon le choix
            int choice;
            if (shouldPrintMenu()) {
                console.printMenu();
                choice = console.printMessageAndGetConsoleNumber("Que voulez-vous faire ?");
            } else {
                executeQuitApplication();
                break;
            }

            try {
                CommandLineRunner commandLineRunner = commands.get(choice);
                if (commandLineRunner != null) {
                    commandLineRunner.execute();
                } else {
                    console.printMessage("Choix incorrect !");
                }
            } catch (BankAccountNotExistsException | ClientNotExistsException e) {
                console.printMessage("Erreur : " + e.getMessage());
            }
        }

        // Affichage du message de sortie
        console.printClosingMessage();
    }

    private void executeQuitApplication() {
        ConnectionManager.closeConnection();
        isLaunch = false;
    }
}
